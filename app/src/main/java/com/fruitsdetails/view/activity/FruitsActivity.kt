package com.fruitsdetails.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.fruitsdetails.R
import com.fruitsdetails.databinding.ActivityFruitsBinding
import com.fruitsdetails.view.adapters.FruitsRvAdapter
import com.fruitsdetails.viewmodel.FruitsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FruitsActivity : AppCompatActivity() {

    private lateinit var fruitsBinding: ActivityFruitsBinding
    private lateinit var fruitsRvAdapter: FruitsRvAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fruits)

        fruitsBinding = ActivityFruitsBinding.inflate(layoutInflater)
        setContentView(fruitsBinding.root)

        initFruitsRecycleView()
        initViewModel()


    }

    private fun initFruitsRecycleView(){

        fruitsBinding.fruitsRecyclerView.layoutManager = LinearLayoutManager(this)
        fruitsRvAdapter = FruitsRvAdapter()
        fruitsBinding.fruitsRecyclerView.adapter = fruitsRvAdapter

    }

    private fun initViewModel(){


      val viewModel: FruitsViewModel =  ViewModelProvider(this).get(FruitsViewModel::class.java)

      viewModel.getLiveDataObserver().observe(this  ,  Observer{
          if (it != null) {
                fruitsRvAdapter.setListData(it)
                fruitsRvAdapter.notifyDataSetChanged()
          }else{
              Toast.makeText(this, "error to get data from server", Toast.LENGTH_SHORT).show()
          }

      })
        viewModel.loadListOfData()
    }
}