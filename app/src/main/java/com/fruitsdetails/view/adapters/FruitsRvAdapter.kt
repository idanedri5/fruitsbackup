package com.fruitsdetails.view.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fruitsdetails.R
import com.fruitsdetails.model.entities.FruitDetails
import kotlinx.android.synthetic.main.fruits_row.view.*

class FruitsRvAdapter()
    : RecyclerView.Adapter<FruitsRvAdapter.ViewHolder>() {

    private var listData: List<FruitDetails>? = null

    fun setListData(listData: List<FruitDetails>?) {
        this.listData = listData
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): FruitsRvAdapter.ViewHolder {
       val view =  LayoutInflater.from(parent.context).inflate(R.layout.fruits_row,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: FruitsRvAdapter.ViewHolder, position: Int) {
        listData?.let {
            holder.bind(listData!![position])
        }

    }

    override fun getItemCount(): Int {

        if(listData == null || listData?.size == 0) {
            return 1
        }else{
           listData?.let {
               return listData!!.size
           }
        }
        return 0
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

       private val fruitName: TextView = view.fruit_name
       private val fruitImage: ImageView = view.fruit_image

        fun bind(data: FruitDetails) {
            fruitName.text = data.name
            Glide.with(fruitImage)
                .load(data.image)
                .into(fruitImage)
        }

    }
}