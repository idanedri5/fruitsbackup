package com.fruitsdetails.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class FruitsApp: Application() {
}