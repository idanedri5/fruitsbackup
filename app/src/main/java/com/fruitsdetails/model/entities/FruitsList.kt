package com.fruitsdetails.model.entities

    data class FruitsApiData(
        val err: Int,
        val fruits : List<FruitDetails>
    )

    data class FruitDetails(
        val description : String?,
        val image: String?,
        val name: String?,
        val price: Int?
    )



