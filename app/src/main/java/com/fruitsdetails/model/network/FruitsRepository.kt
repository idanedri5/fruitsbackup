package com.fruitsdetails.model.network


import androidx.lifecycle.MutableLiveData
import com.fruitsdetails.model.entities.FruitDetails
import com.fruitsdetails.model.entities.FruitsApiData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.Query
import javax.inject.Inject

class FruitsRepository @Inject constructor(private val fruitsInstance: FruitsServiceInstance) {

    fun makeFruitsApiCall( liveData: MutableLiveData<List<FruitDetails>>){
        val call: Call<FruitsApiData> =  fruitsInstance.getFruitsFromApi()
        call?.enqueue(object : Callback<FruitsApiData> {
            override fun onResponse(call: Call<FruitsApiData>, response: Response<FruitsApiData>) {
                response.body()?.let {
                    liveData.postValue(response.body()?.fruits)
                }

            }

            override fun onFailure(call: Call<FruitsApiData>, t: Throwable) {
                liveData.postValue(null)
            }

        })
    }

}